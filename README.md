# **Animal Crossing Market · Front**

[![node.js](https://img.shields.io/static/v1?label=Node.js&message=v12.15&color=339933&style=flat-square&logo=node.js&logoColor=white)](https://nodejs.org/en/)
[![yarn](https://img.shields.io/static/v1?label=Yarn&message=v1.22&color=2C8EBB&style=flat-square&logo=yarn&logoColor=white)](https://classic.yarnpkg.com/lang/en/)

[![vue.js](https://img.shields.io/static/v1?label=Vue.js&message=v2.6&color=4fc08d&style=flat-square&logo=vue.js&logoColor=white)](https://vuejs.org/)
[![typescript](https://img.shields.io/static/v1?label=TypeScript&message=v3.9&color=007ACC&style=flat-square&logo=typescript&logoColor=white)](https://www.typescriptlang.org/)
[![eslint](https://img.shields.io/static/v1?label=ESLint&message=v6.7&color=4B32C3&style=flat-square&logo=eslint&logoColor=white)](https://eslint.org/)

[![tailwindcss](https://img.shields.io/static/v1?label=Tailwind%20CSS&message=v1.4&color=38B2AC&style=flat-square&logo=tailwind-css&logoColor=white)](https://tailwindcss.com/)


## **I. Project setup**
```
yarn
```

### ***I. a. For development***

**Compiles and hot-reloads**

```
yarn dev
```

### ***I. b. For production***

**Compiles and minifies**

```
yarn build
```

**Lints and fixes files**

```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
