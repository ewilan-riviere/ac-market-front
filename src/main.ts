import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
// for PWA feature
import './registerServiceWorker'
import router from './router'
import store from './store'

/**
 * Tailwind CSS v1.4
 * https://tailwindcss.com/
 */
import '@/assets/styles/tailwind.css'

/**
 * PLUGINS
 */
/**
 * Global Components Loader
 * Auto load all components in src/components/tools directory
 */
import '@/plugins/global-components-loader'
/**
 * Vue icon loader
 * Auto load all SVG files for icon-base.vue component in src/assets/icons
 */
import '@/plugins/vue-icons-loader'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
